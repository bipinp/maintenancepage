#!/bin/bash
creset=`tput sgr0`
cwd=`pwd`
mac="PLOG=$cwd/update-server.log"
e="$HOME" #find user home dir
u="$USER" #find user name 
sub=`which python2`
sub1="/usr/bin/python2"
pop="/usr/bin/pip"
pop1=`which pip`

#echo -n -e  "Enter the odoo port : $creset"
#read OE_VERSION
#sudo sed -i "s/application.listen(80)/application.listen($OE_VERSION)/g" $cwd/manage_route.py

if cat /etc/*release | grep ^NAME | grep CentOS; then
echo -n -e   '\E[32m' "===============================================\n"
echo -n -e   '\E[32m' "     This is a  CentOS Server                  \n"
echo -n -e   '\E[32m' "===============================================\n"
echo ""
if [ "$sub" == "$sub1"  ]; then
echo "python is found:"
else
echo "python not found,install package:"
sudo  yum update
sudo  yum -y install python-pip
fi

if [ "$pop" == "$pop1"  ]; then
echo "pip is found:"
else
echo "pip not found,install package:"
sudo yum install gcc python-devel 
sudo yum whatprovides netstat
sudo yum install net-tools -y
sudo pip install tornado
fi
elif cat /etc/*release | grep ^NAME | grep Ubuntu; then
echo -n -e   '\E[32m'  "===============================================\n"
echo -n -e   '\E[32m'  "    This is a Ubuntu Server                    \n"
echo -n -e   '\E[32m'  "===============================================\n"
echo ""
if [ "$sub" == "$sub1"  ]; then
echo "python is found:"
else
echo "python not found,install package:"
sudo  apt update
sudo  apt install  python3 python3-dev python3-venv python3-wheel net-tools
fi

if [ "$pop1" == "$pop"  ]; then
echo "pip is found:"
else
echo "pip not found,install package:"
sudo  apt install python3-pip
sudo  pip3 install tornado
fi

###################
# adding log path 
###################

if cat /etc/*release | grep ^NAME | grep CentOS; then
cat <<EOF   > $cwd/update-server.sh
#!/bin/bash
$mac
cd  $cwd
sudo python manage_route.py  >>  \$PLOG 
EOF
elif cat /etc/*release | grep ^NAME | grep Ubuntu; then
cat <<EOF   > $cwd/update-server.sh
#!/bin/bash
$mac
cd  $cwd
sudo python3 manage_route.py  >>  \$PLOG 
EOF
else
echo -n -e '\E[31m'  "OS NOT DETECTED, Skip the task\n"
fi

########################
# confinger service file 
########################
echo "editing Some routing_updation file"
sudo sed -i "17 a HPATH=$cwd" "$cwd/services/update-server"
sudo sed -i "18 a LOGFILE=$cwd/update-server.log" "$cwd/services/update-server"
sudo mv $cwd/services/update-server  /etc/init.d/

echo ""
echo -n -e '\E[33m'  "===Module installed=== \n"
echo -e '\E[32m' "#------------------------------------------------------------#"
echo -e          " Start Module       : sudo /etc/init.d/update-server start" 
echo -e          " Stop Module        : sudo /etc/init.d/update-server stop"                           
echo -e          " Restart Module     : sudo /etc/init.d/update-server restart"
echo -e          " Check Module Status: sudo /etc/init.d/update-server status"
echo -e          " #------------------------------------------------------------#"



else
echo -n -e '\E[31m'  "OS NOT DETECTED, Skip the task\n"
fi
echo -n -e   '\E[37m'
echo -n -e   ""
