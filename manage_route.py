#!/usr/bin/env python
import tornado.ioloop
import tornado.web
import tornado
import os
cwd = os.path.abspath('')



class IndexPageHandler(tornado.web.RequestHandler):
    def get(self):
        self.render("index.html")


if __name__ == "__main__":

    
    settings = {
            'template_path': 'templates',
	    "static_path": cwd
        }
    application = tornado.web.Application([
	(r'/', IndexPageHandler),
	(r'/.*', IndexPageHandler)
	
    ],**settings)
    application.listen(80)
    tornado.ioloop.IOLoop.current().start() 
