FROM ubuntu:18.04
ADD requirements.txt /requirements.txt

RUN apt-get update  -y -o Dpkg::Options::="--force-confold" && \
    apt-get install --no-install-recommends --yes python python-dev python-pip net-tools python-setuptools && \
    pip install wheel && \
    pip install -r  requirements.txt && \
    apt-get purge -y --auto-remove  build-essential && \
    rm -rf /var/lib/apt/lists/ && \
    mkdir /maintenancepage 

WORKDIR /maintenancepage 
COPY  ./  ./
EXPOSE 80
VOLUME /maintenancepage
CMD [ "python", "manage_route.py" ]
#ENTRYPOINT [ "sh","/maintenancepage/server.sh"]
